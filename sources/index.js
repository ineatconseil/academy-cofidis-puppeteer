const puppeteer = require('puppeteer');
const Bluebird = require('bluebird');

async function scrap(page) {
	try {
		await page.goto('https://www.google.com', { waitUntil: 'networkidle2' });

		await Bluebird.delay(3000);
	} catch (error) {
		console.log('Navigation fail', error);
	}

	await page.close();
}

(async () => {
	const browser = await puppeteer.launch({
		headless: false,
		devtools: true,
		defaultViewport: { width: 1200, height: 800 },
	});

	try {
		const [ page ] = await browser.pages();
		await scrap(page);
	} catch (error) {
		console.log('Scrapping fail', error);
	}

	await browser.close();
})();
